#!/bin/sh

if [ "x$DJANGO_MANAGEPY_MIGRATE" = 'xon' ]; then
    python manage.py migrate --noinput
fi

python manage.py migrate

python manage.py runserver 0.0.0.0:8000
