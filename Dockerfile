FROM python:3.8.3-alpine

WORKDIR /usr/src

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt .
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev \
    && pip install --upgrade pip \
    && pip install -r requirements.txt


COPY ./article .

COPY ./Blog .

COPY ./entrypoint.sh .

ENTRYPOINT ["sh", "/usr/src/entrypoint.sh"]